from django.contrib import admin
from .models import Comentario

# Register your models here.
class ComentarioAdmin(admin.ModelAdmin):
  readonly_fields = ('created', )

admin.site.register(Comentario, ComentarioAdmin)