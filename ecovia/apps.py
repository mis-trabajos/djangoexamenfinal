from django.apps import AppConfig


class EcoviaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ecovia'
