from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from .models import Comentario

from .forms import ComentarioForm

# Create your views here.


def signup(request):
    if request.method == 'GET':
        return render(request, 'signup.html', {"form": UserCreationForm})
    else:

        if request.POST["password1"] == request.POST["password2"]:
            try:
                user = User.objects.create_user(
                    request.POST["username"], password=request.POST["password1"])
                user.save()
                login(request, user)
                return redirect('comentarios')
            except IntegrityError:
                return render(request, 'signup.html', {"form": UserCreationForm, "error": "Nombre de usuario ya existe."})

        return render(request, 'signup.html', {"form": UserCreationForm, "error": "La contraseña no concuerda."})


@login_required
def comentarios(request):
    comentarios = Comentario.objects.filter(user=request.user, datecompleted__isnull=True)
    return render(request, 'comentarios.html', {"comentarios": comentarios})

@login_required
def comentarios_completed(request):
    comentarios = Comentario.objects.filter(user=request.user, datecompleted__isnull=False).order_by('-datecompleted')
    return render(request, 'comentarios.html', {"comentarios": comentarios})


@login_required
def crear_comentario(request):
    if request.method == "GET":
        return render(request, 'crear_comentario.html', {"form": ComentarioForm})
    else:
        try:
            form = ComentarioForm(request.POST)
            new_comentario = form.save(commit=False)
            new_comentario.user = request.user
            new_comentario.save()
            return redirect('comentarios')
        except ValueError:
            return render(request, 'crear_comentario.html', {"form": ComentarioForm, "error": "Error al crear comentario."})


def home(request):
    return render(request, 'home.html')


@login_required
def signout(request):
    logout(request)
    return redirect('home')


def signin(request):
    if request.method == 'GET':
        return render(request, 'signin.html', {"form": AuthenticationForm})
    else:
        user = authenticate(
            request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'signin.html', {"form": AuthenticationForm, "error": "Nombre de usuario o contraseña es incorrecta."})

        login(request, user)
        return redirect('comentarios')

@login_required
def comentario_detalle(request, comentario_id):
    if request.method == 'GET':
        comentario = get_object_or_404(Comentario, pk=comentario_id, user=request.user)
        form = ComentarioForm(instance=comentario)
        return render(request, 'comentario_detalle.html', {'comentario': comentario, 'form': form})
    else:
        try:
            comentario = get_object_or_404(Comentario, pk=comentario_id, user=request.user)
            form = ComentarioForm(request.POST, instance=comentario)
            form.save()
            return redirect('comentarios')
        except ValueError:
            return render(request, 'comentario_detalle.html', {'comentario': comentario, 'form': form, 'error': 'Error al actualizar comentario.'})

@login_required
def complete_comentario(request, comentario_id):
    comentario = get_object_or_404(Comentario, pk=comentario_id, user=request.user)
    if request.method == 'POST':
        comentario.datecompleted = timezone.now()
        comentario.save()
        return redirect('comentarios')

@login_required
def eliminar_comentario(request, comentario_id):
    comentario = get_object_or_404(Comentario, pk=comentario_id, user=request.user)
    if request.method == 'POST':
        comentario.delete()
        return redirect('comentarios')