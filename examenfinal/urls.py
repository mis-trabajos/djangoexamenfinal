
from django.contrib import admin
from django.urls import path
from ecovia import views

urlpatterns = [
    path('', views.home, name='home'),
    path('admin/', admin.site.urls),
    path('signup/', views.signup, name='signup'),
    path('comentarios/', views.comentarios, name='comentarios'),
    path('comentarios_completed/', views.comentarios_completed, name='comentarios_completed'),
    path('logout/', views.signout, name='logout'),
    path('signin/', views.signin, name='signin'),
    path('crear_comentario/', views.crear_comentario, name='crear_comentario'),
    path('comentario/<int:comentario_id>', views.comentario_detalle, name='comentario_detalle'),
    path('comentarios/<int:comentario_id>/complete', views.complete_comentario, name='complete_comentario'),
    path('comentario/<int:comentario_id>/eliminar', views.eliminar_comentario, name='eliminar_comentario'),
]